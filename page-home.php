<?php
/**
 * Template Name: Page - Home
 *
 *
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<div id="slide1" class="row masthead">

<div class="tint"></div>

<picture>
<source srcset="<?php echo get_template_directory_uri(); ?>/img/feature-bg.jpg" media="(min-width: 1501px)">
<source srcset="<?php echo get_template_directory_uri(); ?>/img/feature-bg-large.jpg" media="(min-width: 1201px)">
<source srcset="<?php echo get_template_directory_uri(); ?>/img/feature-bg-medium.jpg" media="(min-width: 941px)">
<source srcset="<?php echo get_template_directory_uri(); ?>/img/feature-bg-standard.jpg" media="(min-width: 801px)">
<source srcset="<?php echo get_template_directory_uri(); ?>/img/feature-bg-small.jpg" media="(min-width: 605px)">
<source srcset="<?php echo get_template_directory_uri(); ?>/img/feature-bg-extra-small.jpg" media="(min-width: 501px)">
<source srcset="<?php echo get_template_directory_uri(); ?>/img/feature-bg-x-x-small.jpg" media="(min-width: 401px)">
<source srcset="<?php echo get_template_directory_uri(); ?>/img/feature-bg-x-x-x-small.jpg" media="(min-width: 100px)">
<img class="bg" src="<?php echo get_template_directory_uri(); ?>/img/feature-bg.jpg"
alt="">
</picture>
    
    <div id="feature-text" class="col-sm-12">
    	<div class="row">
			<div class="col-sm-8 col-sm-offset-2"><?php the_content(); ?></div>
        </div>
        
        <div class="col-sm-12">
        <a href="http://www.four-elements-web-design.co.uk/start-your-project/?utm_source=Website&utm_medium=Button&utm_campaign=Home%20Page%20Mobile%20-%20CTA&utm_term=Home%20Page%20Mobile%20-%20CTA&utm_content=Home%20Page%20Mobile%20-%20CTA" id="mobile-start-project" class="start-your-project">START YOUR PROJECT</a>
        <a id="hexagon" href="http://www.four-elements-web-design.co.uk/start-your-project/?utm_source=Website&utm_medium=Button&utm_campaign=Home%20Page%20Hex%20-%20CTA&utm_term=Home%20Page%20Hex%20-%20CTA&utm_content=Home%20Page%20Hex%20-%20CTA">
<!-- Generator: Adobe Illustrator 19.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg id="hexagon-svg" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 190 215" style="enable-background:new 0 0 190 215;" xml:space="preserve">
<style type="text/css">
	.st0{fill:none;stroke:#fff;stroke-miterlimit:10;}
	.st1{fill:#fff;}
	.st2{font-family:'Open Sans';}
	.st3{font-size:22.2245px;}
</style>
<polygon class="st0" points="5.5,55.3 94.5,4 183.5,55.3 183.5,157.9 94.5,209.2 5.5,157.9 "/>
<text style="font-family:'open sans';" transform="matrix(1 0 0 1 29.9814 100)"><tspan x="0" y="0" class="st1 st2 st3">START YOUR</tspan><tspan x="20.5" y="33.3" class="st1 st2 st3">PROJECT</tspan></text>
</svg>
        </a>
        
        <div class="scroll-link col-sm-12">
        	<div class="line">
            <a href="#recent-text"><svg id="arrow" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 190 215" style="enable-background:new 0 0 190 215;" xml:space="preserve">
<style type="text/css">
	.st0{fill:none;stroke:#ffffff;stroke-miterlimit:10;}
</style>
<circle class="st0" cx="96" cy="109" r="85"/>
<g>
	<line class="st0" x1="97.5" y1="41" x2="97.5" y2="178"/>
	<line class="st0" x1="97.4" y1="178.2" x2="139" y2="136.6"/>
	<line class="st0" x1="97.1" y1="178.2" x2="55.6" y2="136.6"/>
</g>
</svg></a>

        </div>
        </div>
    </div>
    </div>
</div>

<div data-animate-down="ha-header-small" data-animate-up="ha-header-large" class="row recent-work ha-waypoint">
	<div id="recent-text" class="col-sm-12">
    	<h2>A Collection of <strong>Recent Work</strong></h2>
		<p>A showcase of projects from Responsive Web Design, logo design and branding to Web application development and online marketing.</p>
    </div>
</div>

<div class="container-fluid">
<div id="recent-work-items" class="row">
	<div class="col-sm-10 col-sm-offset-1">
    	<div class="row">
        <?php
                
        $args = array(
        'post_type' => 'portfolio',
        'posts_per_page' => 3,
		'orderby' => 'menu_order',
		'order' => 'ASC'
         );
        
        $loop = new WP_Query( $args );
        
        $count = $loop->post_count;
        
        
        while ( $loop->have_posts() ) : $loop->the_post(); ?>
        
        <div class="col-sm-4">
        <a href="<?php the_permalink();?>" title="<?php the_title(); ?>">
        <?php the_post_thumbnail('large'); ?>
        </a>
        </div>
        
        <?php endwhile; ?>
        </div>
        
        <div class="row">
        <a class="ghost-button" href="http://www.four-elements-web-design.co.uk/work/?utm_source=Website&utm_medium=Button&utm_campaign=Home%20Page%20Potfolio%20-%20CTA&utm_term=Home%20Page%20Potfolio%20-%20CTA&utm_content=Home%20Page%20Potfolio%20-%20CTA">View Portfolio</a>
        </div>
        
    </div>
</div>
</div>
</div>

<div id="services" class="container-fluid">
     <div class="row">
     <h2 class="col-sm-12">From idea to reality - <strong>Services</strong> </h2>	
	</div>
	
	<div class="row">
	<div id="design" class="col-sm-3 equale">
        <!--<div class="image"></div>-->
	    <h3>Web / Graphic / Interface Design</h3>
        
        <?php echo file_get_contents(TEMPLATEPATH."/img/web-design.svg"); ?>

	    <div class="button-container"><a href="http://www.four-elements-web-design.co.uk/services/?utm_source=Website&utm_medium=Button&utm_campaign=Home%20Page%20Service%20-%20Web%20Design%20-%20CTA&utm_term=Home%20Page%20Service%20-%20Web%20Design%20-%20CTA&utm_content=Home%20Page%20Service%20-%20Web%20Design%20-%20CTA" class="minimal-button">Find Out More</a></div>
	</div>	
    <div id="development" class="col-sm-3 equale">
        <!--<div class="image"></div>-->
	    <h3>Web / App Development</h3>
        
        <?php echo file_get_contents(TEMPLATEPATH."/img/web-dev.svg"); ?>
        
        <div class="button-container"><a href="http://www.four-elements-web-design.co.uk/services/?utm_source=Website&utm_medium=Button&utm_campaign=Home%20Page%20Service%20-%20Web%20Development%20-%20CTA&utm_term=Home%20Page%20Service%20-%20Web%20Development%20-%20CTA&utm_content=Home%20Page%20Service%20-%20Web%20Development%20-%20CTA" class="minimal-button">Find Out More</a></div>
	</div>
	<div id="branding" class="col-sm-3 equale">
        <!--<div class="image"></div>-->
	    <h3>Branding and Identity</h3>
        
        <?php echo file_get_contents(TEMPLATEPATH."/img/branding.svg"); ?>
        
        <div class="button-container"><a href="http://www.four-elements-web-design.co.uk/services/?utm_source=Website&utm_medium=Button&utm_campaign=Home%20Page%20Service%20-%20Branding%20-%20CTA&utm_term=Home%20Page%20Service%20-%20Branding%20-%20CTA&utm_content=Home%20Page%20Service%20-%20Branding%20-%20CTA" class="minimal-button">Find Out More</a></div>
	</div>
    <div id="marketing" class="col-sm-3 equale">
       <!--<div class="image"></div>-->
	   <h3>Online Marketing, Social and SEO</h3>
       
       <?php echo file_get_contents(TEMPLATEPATH."/img/marketing.svg"); ?>
       
       <div class="button-container"><a href="http://www.four-elements-web-design.co.uk/services/?utm_source=Website&utm_medium=Button&utm_campaign=Home%20Page%20Service%20-%20Marketing%20-%20CTA&utm_term=Home%20Page%20Service%20-%20Marketing%20-%20CTA&utm_content=Home%20Page%20Service%20-%20Marketing%20-%20CTA" class="minimal-button">Find Out More</a></div>
	</div>
	</div>
	
    </div>
    
    <div class="container-fluid">
    <div class="row" id="testimonial">
    	<div class="col-sm-12">
        
        <?php
                
        $args = array(
        'post_type' => 'testimonial',
        'posts_per_page' => 1,
		'orderby' => 'rand',
		'order' => 'DSC'
            );
        
        $loop = new WP_Query( $args );
        
        $count = $loop->post_count;
        
        
        while ( $loop->have_posts() ) : $loop->the_post(); ?>
      
      
       <?php the_excerpt(); ?>
       <p class="testimonial-name"><?php the_title(); ?></p>

       
       <?php endwhile; ?>
        
        </div>
    </div>
    </div>

	<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>
