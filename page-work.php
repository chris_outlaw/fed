<?php
/*
Template Name: Work Page
*/
get_header(); ?>


<div class="row work-header">
<div class="text-header masthead">
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

          <h2 class="col-sm-12">A Collection of <strong>Recent Work</strong></h2>
          
    <?php endwhile; endif; ?>
</div>
</div>

   <?php
                
        $args = array(
        'post_type' => 'portfolio',
        'posts_per_page' => -1,
	'orderby' => 'menu_order',
	'order' => 'ASC'
            );
        
        $loop = new WP_Query( $args );
        
        $count = $loop->post_count;
        
        $port = 1;
        
        
        while ( $loop->have_posts() ) : $loop->the_post(); ?>
        
        <?php if($port == 1) { ?>
        <div data-animate-down="ha-header-small" data-animate-up="ha-header-large" id="portfolio-items" class="container-fluid ha-waypoint">
        <div id="firstImagerow" class="row">
          <div id="firstImage" class="col-sm-12">
          	<a href="<?php the_permalink(); ?>">
            <?php the_post_thumbnail("full"); ?>
            <?php echo types_render_field('full-width-image'); ?>
            <div class="hover-box">
                <h3><?php the_title(); ?></h3>
            <div class="row">
                <ul>
                <?php $terms = get_the_terms( $post->ID, 'portfolio-type' );
				
								
				foreach ($terms as $term) {
					echo "<li class=\"col-sm-12\">".$term->name."</li>";
				}
			
				?>
                </ul>
            </div>
                <p class="ghost-button">View Project</p>
            </div>
            </a>
          </div>
        </div>
        </div>
        
        <div class="container-fluid portfolio-items">
        <div class="row">
        <?php } else { ?>
        
        <?php if($port == 5 || $port == 8 || $port == 11 || $port == 14 || $port == 17) { ?>
        
        <div class="row">
        
        <?php } ?>
        
        <div class="col-sm-4">
          <a href="<?php the_permalink(); ?>">
          <?php the_post_thumbnail("full"); ?>
          <div class="hover-box">
          	<h3><?php the_title(); ?></h3>
            <div class="row">
             <ul>
                <?php $terms = get_the_terms( $post->ID, 'portfolio-type' ); 
				
								
				foreach ($terms as $term) {
					echo "<li class=\"col-sm-12\">".$term->name."</li>";
				}
			
				?>
                </ul>
            </div>
            <p class="ghost-button">View Project</p>
          </div>
          </a>
        </div>
        
        <?php if($port == 4 || $port == 7 || $port == 10 || $port == 13 || $port == 16 || $port == 19) { ?>
        
        </div>
        
        <?php } ?>
       
        
        <?php } $port++; endwhile; ?>

</div>


<?php get_footer();?>
