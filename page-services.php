<?php
/*
Template Name: Services Page
*/
get_header(); ?>

<div class="jumbotron-normal masthead masthead-services">
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

     <div class="row">
      
      <div id="about-intro" class="col-sm-12">
      <div class="row">
<?php the_content(); ?>
      </div>
     
     
     <div class="scroll-link col-sm-12">
        	<div class="line">
            <a href="#service-arrow-stop"><svg id="arrow" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 190 215" style="enable-background:new 0 0 190 215;" xml:space="preserve">
<style type="text/css">
	.st0{fill:none;stroke:#ffffff;stroke-miterlimit:10;}
</style>
<circle class="st0" cx="96" cy="109" r="85"/>
<g>
	<line class="st0" x1="97.5" y1="41" x2="97.5" y2="178"/>
	<line class="st0" x1="97.4" y1="178.2" x2="139" y2="136.6"/>
	<line class="st0" x1="97.1" y1="178.2" x2="55.6" y2="136.6"/>
</g>
</svg></a>

        </div>
        </div>
     
      
      </div>
      
    </div>
    
</div>

<div id="service-content-container" class="row">
  
  <div data-animate-down="ha-header-small" data-animate-up="ha-header-large" class="col-sm-12 ha-waypoint">
  
  <div id="service-arrow-stop" class="row">
  
  <div class="col-sm-3">
    <div class="row">
    <div id="design" class="col-sm-12">
    <?php echo file_get_contents(TEMPLATEPATH."/img/web-design.svg"); ?>
  	</div>
    </div>
    
    <div class="row">
    <div class="col-sm-12">
    <h2><strong>Web / Graphic / Interface Design</strong></h2>
        
    <a class="ghost-button" href="#webdes">Find Out More</a>
     
  </div>
  </div>
  </div>
  



  <div class="col-sm-3">
  
  <div class="row">
  <div id="development" class="col-sm-12">
    <?php echo file_get_contents(TEMPLATEPATH."/img/web-dev.svg"); ?>
  </div>
  </div>
  
  
  <div class="row">
  <div class="col-sm-12">
    <h2><strong>Web / App Development</strong></h2>
    <a class="ghost-button" href="#webdev">Find Out More</a>
  </div>
   
  </div>
  
</div>


  
  <div class="col-sm-3">
  
  <div class="row">
  <div id="branding" class="col-sm-12">
    <?php echo file_get_contents(TEMPLATEPATH."/img/branding.svg"); ?>
  </div>
  </div>
  
  <div class="row">
  <div class="col-sm-12">
    <h2><strong>Branding &amp; Identity</strong></h2>
    <a class="ghost-button" href="#brand">Find Out More</a>
  </div>
  
  
  </div>
  
</div>


  
  <div class="col-sm-3">
  
  	<div class="row">
    <div id="marketing" class="col-sm-12">
    <?php echo file_get_contents(TEMPLATEPATH."/img/marketing.svg"); ?>
  	</div>
    </div>
  
  <div class="row">
  
  <div class="col-sm-12">
    <h2><strong>Online Marketing</strong></h2>
        
        <a href="#mark" class="ghost-button">Find Out More</a>
        
  </div>
  
  </div>
  
</div>
</div>
</div>

<div id="servicebanners" class="container-fluid">
<div id="service-strip" class="row">

<div id="webdes" class="col-sm-12">

	<div class="row">
    <h3>Web Design, Graphic Design &amp; UI / UX Design</h3>
    <p>Creating eyecatching designs and interfaces with the user in mind.</p>
    </div>
</div>

<div class="container-fluid web-services sub-services">
<div class="col-sm-10 col-sm-offset-1 sub-services">    
    <div class="row">
    	<ul>
        	<li>
            	<div class="row"><img src="<?php echo get_template_directory_uri(); ?>/img/web-design-service.jpg" /></div>
                <div class="row"><h4>Web Design</h4></div>
                <p>Custom built Web sites designed to your needs and aligned to your business goals.</p>
            </li>
            <li>
            <div class="row"><img src="<?php echo get_template_directory_uri(); ?>/img/ui-design-service.jpg" /></div>
                <div class="row"><h4>UI / UX Design</h4></div>
                <p>Creating engaging interfaces and experiences for your web site / application helping the user find what they are looking for.</p>
            </li>
            <li><div class="row"><img src="<?php echo get_template_directory_uri(); ?>/img/responsive-design-service.jpg" /></div>
                <div class="row"><h4>Responsive Design</h4></div>
                <p>Making sure your web site / application is presented the best it can on any device of any size from smart phones and tablets to desktop computers.</p>
            </li>
            <li>
            <div class="row"><img src="<?php echo get_template_directory_uri(); ?>/img/graphic-design-service.jpg" /></div>
                <div class="row"><h4>Graphic Design</h4></div>
                <p>Taking your online presence offline with point of sale, brochures, print advertising and stationary. Allowing for a consistent message accross platforms.</p>
            </li>
        </ul>
    </div>

</div>
</div>

<div class="container-fluid">

<div class="row"><h4>Recent Web Design, Graphic Design &amp; UI / UX Design Projects</h4></div>

<div class="row recent-service-projects">

<div class="col-sm-6 col-sm-offset-3">

 <?php 
 	$args = array(
    'post_type' => 'portfolio',
	'portfolio-type' => 'web-design',
    'posts_per_page' => 2,
	'orderby' => 'menu_order',
	'order' => 'ASC'
     );
        
     $loop = new WP_Query( $args );
        
        
     while ( $loop->have_posts() ) : $loop->the_post(); ?>
     

<div class="col-sm-6">
<?php the_post_thumbnail('full'); ?>
</div>

<?php endwhile; ?>

</div>

</div>

<div class="row"><a href="http://www.four-elements-web-design.co.uk/start-your-project/?utm_source=Website&utm_medium=Button&utm_campaign=Start%20your%20project%20-%20Services%20-%20Web%20Design&utm_term=Start%20your%20project%20-%20Services%20-%20Web%20Design&utm_content=Start%20your%20project%20-%20Services%20-%20Web%20Design" class="start-your-project">Start your project</a></div>

</div>

</div>

<hr />

<div id="service-strip" class="row">

<div id="webdev" class="col-sm-12">

	<div class="row">
    <h3>Web / App Development</h3>
    <p>Fuctional interactive experiences built with care.</p>
    </div>
</div>


<div class="container-fluid web-dev-services sub-services">
<div class="col-sm-10 col-sm-offset-1 sub-services">    
    <div class="row">
    	<ul>
        	<li>
            	<div class="row"><img src="<?php echo get_template_directory_uri(); ?>/img/web-development-service.jpg" /></div>
                <div class="row"><h4>Web Development</h4></div>
                <p>Web Applications to streamline your business processes. Custom built to your needs and specifications.</p>
            </li>
            <li>
            <div class="row"><img src="<?php echo get_template_directory_uri(); ?>/img/ecommerce-development-service.jpg" /></div>
                <div class="row"><h4>Ecommerce Development</h4></div>
                <p>Allowing you to sell your products online safely, securely and efficiently.</p>
            </li>
            <li><div class="row"><img src="<?php echo get_template_directory_uri(); ?>/img/wordpress-development-service.jpg" /></div>
                <div class="row"><h4>Wordpress Themes / Development</h4></div>
                <p>Bespoke Wordpress theme design and customisation as well as plugin development to help you manage your own content.</p>
            </li>
            <li>
            <div class="row"><img src="<?php echo get_template_directory_uri(); ?>/img/app-development-service.jpg" /></div>
                <div class="row"><h4>App Development</h4></div>
                <p>Mobile applications designed and built to your needs and specifications.</p>
            </li>
        </ul>
    </div>

</div>
</div>

<div class="container-fluid">

<div class="row"><h4>Recent Web / App Development Projects</h4></div>

<div class="row recent-service-projects">

<div class="col-sm-6 col-sm-offset-3">

 <?php 
 	$args = array(
    'post_type' => 'portfolio',
	'portfolio-type' => 'web-development',
    'posts_per_page' => 2,
	'orderby' => 'menu_order',
	'order' => 'ASC'
     );
        
     $loop = new WP_Query( $args );
        
        
     while ( $loop->have_posts() ) : $loop->the_post(); ?>
     

<div class="col-sm-6">
<?php the_post_thumbnail('full'); ?>
</div>

<?php endwhile; ?>

</div>

</div>

<div class="row"><a href="http://www.four-elements-web-design.co.uk/start-your-project/?utm_source=Website&utm_medium=Button&utm_campaign=Start%20your%20project%20-%20Services%20-%20Web%20Development&utm_term=Start%20your%20project%20-%20Services%20-%20Web%20Development&utm_content=Start%20your%20project%20-%20Services%20-%20Web%20Development" class="start-your-project">Start your project</a></div>

</div>

</div>

<hr />


<div id="service-strip" class="row">

<div id="brand" class="col-sm-12">

	<div class="row">
    <h3>Branding &amp; Identity</h3>
    <p>Distilling the essence of your business, creating a brand that represents it and presenting it to the world.</p>
    </div>
</div>


<div class="container-fluid brand-services sub-services">
<div class="col-sm-10 col-sm-offset-1 sub-services">    
    <div class="row">
    	<ul>
        	<li>
            	<div class="row"><img src="<?php echo get_template_directory_uri(); ?>/img/logo-design-service.jpg" /></div>
                <div class="row"><h4>Logo Design</h4></div>
                <p>Embodying your businesses identity, values and personality into a memorable logo.</p>
            </li>
            <li>
            <div class="row"><img src="<?php echo get_template_directory_uri(); ?>/img/brand-strategy-service.jpg" /></div>
                <div class="row"><h4>Brand Strategy</h4></div>
                <p>Creating a strategy that connects the dots between your brands Identity and overall interal and external goals, values and tone.</p>
            </li>
            <li><div class="row"><img src="<?php echo get_template_directory_uri(); ?>/img/corporate-identity-service.jpg" /></div>
                <div class="row"><h4>Corporate Identity</h4></div>
                <p>Adding a personality, character and tone of voice that represents your business core values and principles.</p>
            </li>
            <li>
            <div class="row"><img src="<?php echo get_template_directory_uri(); ?>/img/style-guide-service.jpg" /></div>
                <div class="row"><h4>Brand Guidelines</h4></div>
                <p>A set of guidelines encompassing your brands, logo, identity, tone of voice, colour pallete, fonts, taglines / phrases and textures, shapes or visual cues.</p>
            </li>
        </ul>
    </div>

</div>
</div>

<div class="container-fluid">

<div class="row"><h4>Recent Branding &amp; Identity Projects</h4></div>

<div class="row recent-service-projects">

<div class="col-sm-6 col-sm-offset-3">

 <?php 
 	$args = array(
    'post_type' => 'portfolio',
	'portfolio-type' => 'branding',
    'posts_per_page' => 2,
	'orderby' => 'menu_order',
	'order' => 'ASC'
     );
        
     $loop = new WP_Query( $args );
        
        
     while ( $loop->have_posts() ) : $loop->the_post(); ?>
     

<div class="col-sm-6">
<?php the_post_thumbnail('full'); ?>
</div>

<?php endwhile; ?>

</div>

</div>

<div class="row"><a href="http://www.four-elements-web-design.co.uk/start-your-project/?utm_source=Website&utm_medium=Button&utm_campaign=Start%20your%20project%20-%20Services%20-%20Branding&utm_term=Start%20your%20project%20-%20Services%20-%20Branding&utm_content=Start%20your%20project%20-%20Services%20-%20Branding" class="start-your-project">Start your project</a></div>

</div>

</div>

<hr />

<div id="service-strip" class="row">

<div id="brand" class="col-sm-12">

	<div class="row">
    <h3>Marketing</h3>
    <p>Spreading the word strategically whilst maintaining company objectives and brand message.</p>
    </div>
</div>

<div class="container-fluid marketing-services sub-services">
<div class="col-sm-10 col-sm-offset-1 sub-services">    
    <div class="row">
    	<ul>
        	<li>
            	<div class="row"><img src="<?php echo get_template_directory_uri(); ?>/img/content-marketing-service.jpg" /></div>
                <div class="row"><h4>Content Marketing</h4></div>
                <p>Creating and cultivating content that appeals to your target demographic to help you increase traffic, links and social media traction.</p>
            </li>
            <li>
            <div class="row"><img src="<?php echo get_template_directory_uri(); ?>/img/social-media-service.jpg" /></div>
                <div class="row"><h4>Social Media Marketing</h4></div>
                <p>Maintaining and managing social channels to increase customer outreach, fanbase and drive traffic to your web site.</p>
            </li>
            <li><div class="row"><img src="<?php echo get_template_directory_uri(); ?>/img/seo-service.jpg" /></div>
                <div class="row"><h4>Search Engine Optimisation</h4></div>
                <p>Optimizing your site for search engines with on page seo, link building, content creation and page speed. To drive traffic and rank higher in search results.</p>
            </li>
            <li>
            <div class="row"><img src="<?php echo get_template_directory_uri(); ?>/img/email-marketing-service.jpg" /></div>
                <div class="row"><h4>E-mail Marketing</h4></div>
                <p>Building your mailing list and consistently using this channel to present offers, new content and events.</p>
            </li>
        </ul>
    </div>

</div>
</div>

<div class="container-fluid">

<div class="row"><h4>Recent Marketing Projects</h4></div>

<div class="row recent-service-projects">

<div class="col-sm-6 col-sm-offset-3">

 <?php 
 	$args = array(
    'post_type' => 'portfolio',
	'portfolio-type' => 'online-marketing',
    'posts_per_page' => 2,
	'orderby' => 'menu_order',
	'order' => 'ASC'
     );
        
     $loop = new WP_Query( $args );
        
        
     while ( $loop->have_posts() ) : $loop->the_post(); ?>
     

<div class="col-sm-6">
<?php the_post_thumbnail('full'); ?>
</div>

<?php endwhile; ?>

</div>

</div>

<div class="row"><a href="http://www.four-elements-web-design.co.uk/start-your-project/?utm_source=Website&utm_medium=Button&utm_campaign=Start%20your%20project%20-%20Services%20-%20Marketing&utm_term=Start%20your%20project%20-%20Services%20-%20Marketing&utm_content=Start%20your%20project%20-%20Services%20-%20Marketing" class="start-your-project">Start your project</a></div>

</div>

</div>


<?php endwhile; endif; ?>

 <div class="row" id="testimonial">
    	<div class="col-sm-12">
        
        <?php
                
        $args = array(
        'post_type' => 'testimonial',
        'posts_per_page' => 1,
		'orderby' => 'rand',
		'order' => 'ASC'
            );
        
        $loop = new WP_Query( $args );
        
        $count = $loop->post_count;
        
        
        while ( $loop->have_posts() ) : $loop->the_post(); ?>
      
      
       <?php the_excerpt(); ?>
       <p class="testimonial-name"><?php the_title(); ?></p>

       
       <?php endwhile; ?>
        
        </div>
    </div>

<?php get_footer();?>