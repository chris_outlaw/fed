<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package _tk
 */
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->

<head>
	
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="p:domain_verify" content="20ed187495a7fe38fbb9c59e56beb580"/>

	<title><?php wp_title( '|', true, 'right' ); ?></title>

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	
	
	<!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="<?php bloginfo( 'template_url' );?>/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php bloginfo( 'template_url' );?>/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php bloginfo( 'template_url' );?>/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php bloginfo( 'template_url' );?>/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php bloginfo( 'template_url' );?>/ico/apple-touch-icon-57-precomposed.png">
    
    
    <!--[if lt IE 9]>
<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->


<link href='https://fonts.googleapis.com/css?family=Oswald:400,700,300' rel='stylesheet' type='text/css'>


<?php wp_head(); ?>


    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,800italic,300italic,600italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/includes/js/picturefill.js"></script>

<script src="<?php echo get_template_directory_uri();?>/includes/js/matchMedia.js"></script>

<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/includes/js/waypoints.min.js"></script>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');

fbq('init', '1580519275601109');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1580519275601109&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

</head>

<body id="top" <?php body_class(); ?>>
	<?php do_action( 'before' ); ?>

<div class="ip-container">

<div id="site-wrap">

<header id="masthead" class="site-header ip-header" role="banner">

	<div class="container-fluid">
		<div class="row">
			<div class="site-header-inner col-sm-3 ip-logo">
					
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
						<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 515.49 149.46"><defs><style>.cls-1{opacity:0.77;}.cls-2{fill:#fff;}.cls-10,.cls-12,.cls-13,.cls-15,.cls-3,.cls-6,.cls-8{opacity:0.2;}.cls-4{fill:url(#linear-gradient);}.cls-5{fill:#149a4b;}.cls-6{fill:url(#linear-gradient-2);}.cls-7{fill:#68bd45;}.cls-8{fill:url(#linear-gradient-3);}.cls-9{fill:#146e87;}.cls-10{fill:url(#linear-gradient-4);}.cls-11{fill:#57c3bc;}.cls-12{fill:url(#linear-gradient-5);}.cls-13{fill:url(#linear-gradient-6);}.cls-14{fill:#32817b;}.cls-15{fill:url(#linear-gradient-7);}</style><linearGradient id="linear-gradient" x1="31.17" y1="103.24" x2="160.6" y2="103.24" gradientTransform="translate(-31.17 -28.51)" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#fff"/><stop offset="1"/></linearGradient><linearGradient id="linear-gradient-2" x1="186.06" y1="87.56" x2="315.5" y2="87.56" gradientTransform="translate(-186.06 -12.83)" xlink:href="#linear-gradient"/><linearGradient id="linear-gradient-3" x1="186.06" y1="124.92" x2="315.5" y2="124.92" gradientTransform="translate(-186.06 -12.83)" xlink:href="#linear-gradient"/><linearGradient id="linear-gradient-4" x1="186.06" y1="50.19" x2="315.5" y2="50.19" gradientTransform="translate(-186.06 -12.83)" xlink:href="#linear-gradient"/><linearGradient id="linear-gradient-5" x1="186.51" y1="68.88" x2="251.23" y2="68.88" gradientTransform="translate(-186.06 -12.83)" xlink:href="#linear-gradient"/><linearGradient id="linear-gradient-6" x1="250.78" y1="106.24" x2="315.5" y2="106.24" gradientTransform="translate(-186.06 -12.83)" xlink:href="#linear-gradient"/><linearGradient id="linear-gradient-7" x1="186.06" y1="106.24" x2="250.78" y2="106.24" gradientTransform="translate(-186.06 -12.83)" xlink:href="#linear-gradient"/></defs><title>four-elements-logo</title><g class="cls-1"><polygon class="cls-2" points="65.17 0.26 65.17 0 64.94 0.13 64.72 0 0 37.37 0 112.09 64.72 149.46 129.43 112.09 129.43 37.37 65.17 0.26"/><g class="cls-3"><polygon class="cls-4" points="65.17 0.26 65.17 0 64.94 0.13 64.72 0 0 37.37 0 112.09 64.72 149.46 129.43 112.09 129.43 37.37 65.17 0.26"/></g></g><g class="cls-1"><polygon class="cls-5" points="129.43 112.09 64.72 149.46 0 112.09 0 37.37 64.72 0 129.43 37.37 129.43 112.09"/><polygon class="cls-6" points="129.43 112.09 64.72 149.46 0 112.09 0 37.37 64.72 0 129.43 37.37 129.43 112.09"/></g><g class="cls-1"><polygon class="cls-7" points="64.72 74.73 0 112.09 64.72 149.46 129.43 112.09 64.72 74.73"/><polygon class="cls-8" points="64.72 74.73 0 112.09 64.72 149.46 129.43 112.09 64.72 74.73"/></g><g class="cls-1"><polygon class="cls-9" points="64.72 0 0 37.37 64.72 74.73 129.43 37.37 64.72 0"/><polygon class="cls-10" points="64.72 0 0 37.37 64.72 74.73 129.43 37.37 64.72 0"/></g><g class="cls-1"><polygon class="cls-11" points="65.17 0 0.45 37.37 0.45 112.09 65.17 74.73 65.17 0"/><polygon class="cls-12" points="65.17 0 0.45 37.37 0.45 112.09 65.17 74.73 65.17 0"/></g><g class="cls-1"><polygon class="cls-11" points="129.43 37.37 64.72 74.73 64.72 149.46 129.43 112.09 129.43 37.37"/><polygon class="cls-13" points="129.43 37.37 64.72 74.73 64.72 149.46 129.43 112.09 129.43 37.37"/></g><g class="cls-1"><polygon class="cls-14" points="64.72 74.73 0 37.37 0 112.09 64.72 149.46 64.72 74.73"/><polygon class="cls-15" points="64.72 74.73 0 37.37 0 112.09 64.72 149.46 64.72 74.73"/></g><path class="cls-2" d="M200.62,103.58a13.21,13.21,0,0,1,.77-4.74,8.18,8.18,0,0,1,2.17-3.24,8.79,8.79,0,0,1,3.31-1.84,14.31,14.31,0,0,1,4.21-.59q4.18,0,5.92,1a2.91,2.91,0,0,1,1.73,2.52,2.52,2.52,0,0,1-.48,1.53,3.93,3.93,0,0,1-1,1A6.3,6.3,0,0,0,215,98.18a10.64,10.64,0,0,0-3-.41q-3,0-4.28,1.53a6.59,6.59,0,0,0-1.28,4.33v1.53h7.65a3.29,3.29,0,0,1,2.37.69,2.73,2.73,0,0,1,.69,2,4.9,4.9,0,0,1-.25,1.61,9.77,9.77,0,0,1-.41,1h-10v20.25h-5.81V103.58Z" transform="translate(-49.17 -36.51)"/><path class="cls-2" d="M234.38,104.45a14.9,14.9,0,0,1,5.53,1,13.63,13.63,0,0,1,4.46,2.83,13.07,13.07,0,0,1,3,4.28,13.4,13.4,0,0,1,1.07,5.38,13.26,13.26,0,0,1-1.07,5.33,13.05,13.05,0,0,1-3,4.28,13.94,13.94,0,0,1-4.46,2.86,15.17,15.17,0,0,1-11.07,0,13.93,13.93,0,0,1-4.46-2.86,13,13,0,0,1-3-4.28,13.26,13.26,0,0,1-1.07-5.33,13.4,13.4,0,0,1,1.07-5.38,13.06,13.06,0,0,1,3-4.28,13.62,13.62,0,0,1,4.46-2.83A14.89,14.89,0,0,1,234.38,104.45Zm0,21.88a8,8,0,0,0,3.21-.64,7.25,7.25,0,0,0,2.52-1.78,8.47,8.47,0,0,0,1.63-2.65,9,9,0,0,0,.59-3.29,9.4,9.4,0,0,0-.59-3.34,8.05,8.05,0,0,0-1.63-2.68,7.44,7.44,0,0,0-2.52-1.76,8.48,8.48,0,0,0-6.45,0,7.49,7.49,0,0,0-2.5,1.76,8,8,0,0,0-1.63,2.68,9.39,9.39,0,0,0-.59,3.34,9,9,0,0,0,.59,3.29,8.45,8.45,0,0,0,1.63,2.65,7.3,7.3,0,0,0,2.5,1.78A8,8,0,0,0,234.38,126.33Z" transform="translate(-49.17 -36.51)"/><path class="cls-2" d="M255.85,107.87a2.46,2.46,0,0,1,.82-2.07,3.65,3.65,0,0,1,2.29-.64,5.84,5.84,0,0,1,1.68.23q0.77,0.23,1,.33v13.41a7.63,7.63,0,0,0,1.63,5.28,7.8,7.8,0,0,0,10,0,7.64,7.64,0,0,0,1.63-5.28V107.87a2.46,2.46,0,0,1,.82-2.07,3.56,3.56,0,0,1,2.24-.64,6,6,0,0,1,1.73.23q0.76,0.23,1,.33v14.69a11.67,11.67,0,0,1-.89,4.67,9.54,9.54,0,0,1-2.55,3.49,11.26,11.26,0,0,1-4,2.17,17.61,17.61,0,0,1-10.1,0,11,11,0,0,1-3.93-2.17,9.93,9.93,0,0,1-2.55-3.49,11.38,11.38,0,0,1-.92-4.67V107.87Z" transform="translate(-49.17 -36.51)"/><path class="cls-2" d="M289.26,114.29a9.24,9.24,0,0,1,2.88-7.27q2.88-2.57,8-2.58a13,13,0,0,1,5.23.89,2.9,2.9,0,0,1,2,2.73,3.25,3.25,0,0,1-.48,1.68,3.4,3.4,0,0,1-.79,1,14,14,0,0,0-2.14-.76,10.81,10.81,0,0,0-2.91-.36q-6,0-6,6.32v14.79h-5.81V114.29Z" transform="translate(-49.17 -36.51)"/><path class="cls-2" d="M351.17,128.88q-0.41.26-1.27,0.71a13,13,0,0,1-2.17.87,24.6,24.6,0,0,1-3,.71,21.37,21.37,0,0,1-3.75.31,16,16,0,0,1-5.33-.87,12.38,12.38,0,0,1-4.33-2.58,12.12,12.12,0,0,1-2.93-4.18,14.2,14.2,0,0,1-1.07-5.69,14.46,14.46,0,0,1,1-5.58,12.78,12.78,0,0,1,2.91-4.33,13.1,13.1,0,0,1,4.39-2.8,15,15,0,0,1,5.53-1,14.66,14.66,0,0,1,4.69.69,10.87,10.87,0,0,1,3.44,1.84,7.51,7.51,0,0,1,2.83,5.94,7.71,7.71,0,0,1-.66,3.24,5.85,5.85,0,0,1-2.35,2.5,14,14,0,0,1-4.54,1.63,37,37,0,0,1-7.24.59l-1.91,0-1.91-.08A6.33,6.33,0,0,0,336,125a9.83,9.83,0,0,0,6,1.61,13.65,13.65,0,0,0,2.45-.2,13.85,13.85,0,0,0,2-.51,14.48,14.48,0,0,0,1.53-.61l0.94-.46Zm-17.9-12.09q1.17,0.05,2.24.05h2.14a23.3,23.3,0,0,0,4.54-.36,9.19,9.19,0,0,0,2.7-.92,2.86,2.86,0,0,0,1.27-1.27,3.71,3.71,0,0,0,.31-1.48,3.2,3.2,0,0,0-1.56-2.91,7.16,7.16,0,0,0-3.85-1,7.81,7.81,0,0,0-3.32.66,7.19,7.19,0,0,0-2.4,1.76,8,8,0,0,0-1.5,2.52A9.15,9.15,0,0,0,333.27,116.79Z" transform="translate(-49.17 -36.51)"/><path class="cls-2" d="M359.28,95.88a2.47,2.47,0,0,1,.82-2,3.55,3.55,0,0,1,2.29-.66,5.83,5.83,0,0,1,1.68.23q0.76,0.23,1,.33v37h-5.81V95.88Z" transform="translate(-49.17 -36.51)"/><path class="cls-2" d="M396.66,128.88q-0.41.26-1.27,0.71a13,13,0,0,1-2.17.87,24.6,24.6,0,0,1-3,.71,21.37,21.37,0,0,1-3.75.31,16,16,0,0,1-5.33-.87A12.38,12.38,0,0,1,376.8,128a12.12,12.12,0,0,1-2.93-4.18,14.2,14.2,0,0,1-1.07-5.69,14.46,14.46,0,0,1,1-5.58,12.78,12.78,0,0,1,2.91-4.33,13.1,13.1,0,0,1,4.39-2.8,15,15,0,0,1,5.53-1,14.66,14.66,0,0,1,4.69.69A10.87,10.87,0,0,1,394.8,107a7.51,7.51,0,0,1,2.83,5.94,7.71,7.71,0,0,1-.66,3.24,5.85,5.85,0,0,1-2.35,2.5,14,14,0,0,1-4.54,1.63,37,37,0,0,1-7.24.59l-1.91,0-1.91-.08a6.33,6.33,0,0,0,2.52,4.21,9.83,9.83,0,0,0,6,1.61,13.65,13.65,0,0,0,2.45-.2,13.85,13.85,0,0,0,2-.51,14.48,14.48,0,0,0,1.53-.61l0.94-.46Zm-17.9-12.09q1.17,0.05,2.24.05h2.14a23.3,23.3,0,0,0,4.54-.36,9.19,9.19,0,0,0,2.7-.92,2.86,2.86,0,0,0,1.27-1.27,3.71,3.71,0,0,0,.31-1.48,3.2,3.2,0,0,0-1.56-2.91,7.16,7.16,0,0,0-3.85-1,7.81,7.81,0,0,0-3.32.66,7.19,7.19,0,0,0-2.4,1.76,8,8,0,0,0-1.5,2.52A9.15,9.15,0,0,0,378.76,116.79Z" transform="translate(-49.17 -36.51)"/><path class="cls-2" d="M404.52,114.29a9.92,9.92,0,0,1,.87-4.28,8.62,8.62,0,0,1,2.37-3.06,10.54,10.54,0,0,1,3.54-1.86,14.82,14.82,0,0,1,4.44-.64,13.17,13.17,0,0,1,5.66,1.17,9.39,9.39,0,0,1,4,3.47,9.44,9.44,0,0,1,3.93-3.47,12.71,12.71,0,0,1,5.56-1.17,15.21,15.21,0,0,1,4.39.61,9.92,9.92,0,0,1,3.52,1.84,8.78,8.78,0,0,1,2.35,3.06,9.93,9.93,0,0,1,.87,4.28v16.52h-5.81V116.23q0-3.52-1.58-5a6,6,0,0,0-4.28-1.5,6.06,6.06,0,0,0-4.36,1.68q-1.76,1.68-1.76,5.46v13.92h-5.81V116.84q0-3.77-1.76-5.46a6.62,6.62,0,0,0-8.64-.15q-1.63,1.53-1.63,5v14.48h-5.81V114.29Z" transform="translate(-49.17 -36.51)"/><path class="cls-2" d="M477.24,128.88q-0.41.26-1.27,0.71a13,13,0,0,1-2.17.87,24.6,24.6,0,0,1-3,.71,21.37,21.37,0,0,1-3.75.31,16,16,0,0,1-5.33-.87,12.38,12.38,0,0,1-4.33-2.58,12.12,12.12,0,0,1-2.93-4.18,14.2,14.2,0,0,1-1.07-5.69,14.46,14.46,0,0,1,1-5.58,12.78,12.78,0,0,1,2.91-4.33,13.1,13.1,0,0,1,4.39-2.8,15,15,0,0,1,5.53-1,14.66,14.66,0,0,1,4.69.69,10.87,10.87,0,0,1,3.44,1.84,7.51,7.51,0,0,1,2.83,5.94,7.71,7.71,0,0,1-.66,3.24,5.85,5.85,0,0,1-2.35,2.5,14,14,0,0,1-4.54,1.63,37,37,0,0,1-7.24.59l-1.91,0-1.91-.08a6.33,6.33,0,0,0,2.52,4.21,9.83,9.83,0,0,0,6,1.61,13.65,13.65,0,0,0,2.45-.2,13.85,13.85,0,0,0,2-.51,14.48,14.48,0,0,0,1.53-.61l0.94-.46Zm-17.9-12.09q1.17,0.05,2.24.05h2.14a23.3,23.3,0,0,0,4.54-.36,9.19,9.19,0,0,0,2.7-.92,2.86,2.86,0,0,0,1.27-1.27,3.71,3.71,0,0,0,.31-1.48,3.2,3.2,0,0,0-1.56-2.91,7.16,7.16,0,0,0-3.85-1,7.81,7.81,0,0,0-3.32.66,7.19,7.19,0,0,0-2.4,1.76,8,8,0,0,0-1.5,2.52A9.15,9.15,0,0,0,459.34,116.79Z" transform="translate(-49.17 -36.51)"/><path class="cls-2" d="M485.09,115.31a10.63,10.63,0,0,1,.94-4.56,9.94,9.94,0,0,1,2.6-3.42,11.54,11.54,0,0,1,3.93-2.14,16,16,0,0,1,5-.74,16.24,16.24,0,0,1,5,.74,11.53,11.53,0,0,1,4,2.14,9.67,9.67,0,0,1,2.58,3.42,10.89,10.89,0,0,1,.92,4.56v15.45h-5.81v-14a7.48,7.48,0,0,0-1.68-5.28,7.7,7.7,0,0,0-9.92,0,7.56,7.56,0,0,0-1.66,5.28v14h-5.81V115.31Z" transform="translate(-49.17 -36.51)"/><path class="cls-2" d="M518.6,100.62a2.46,2.46,0,0,1,.82-2.07,3.56,3.56,0,0,1,2.24-.64,6.1,6.1,0,0,1,1.71.23q0.79,0.23,1,.33v6.68h7.4a3.29,3.29,0,0,1,2.37.69,2.73,2.73,0,0,1,.69,2,4.9,4.9,0,0,1-.25,1.61,9.74,9.74,0,0,1-.41,1h-9.79v11.37a5.59,5.59,0,0,0,.36,2.17,3.36,3.36,0,0,0,1,1.35,3.73,3.73,0,0,0,1.48.71,7.43,7.43,0,0,0,1.76.2,8.06,8.06,0,0,0,2.75-.46,7.79,7.79,0,0,0,1.84-.87l2.35,3.88q-0.41.31-1.15,0.77a10.62,10.62,0,0,1-1.81.87,16.25,16.25,0,0,1-2.4.69,14.09,14.09,0,0,1-2.91.28q-4.49,0-6.78-2.32a8.29,8.29,0,0,1-2.29-6.09V100.62Z" transform="translate(-49.17 -36.51)"/><path class="cls-2" d="M542.16,124.39a8,8,0,0,0,1.28.66,22.7,22.7,0,0,0,2.27.84,23.51,23.51,0,0,0,3.09.74,21.74,21.74,0,0,0,3.77.31,11.58,11.58,0,0,0,4.33-.69,2.41,2.41,0,0,0,1.68-2.37,2.47,2.47,0,0,0-1.07-2.17,8.92,8.92,0,0,0-3.82-1l-4-.46a18.71,18.71,0,0,1-3.75-.74A9.64,9.64,0,0,1,543,118a6.47,6.47,0,0,1-1.91-2.27,7.06,7.06,0,0,1-.69-3.24,7.58,7.58,0,0,1,.64-3.09,6.89,6.89,0,0,1,2-2.58,10,10,0,0,1,3.6-1.76,19.71,19.71,0,0,1,5.38-.64,23,23,0,0,1,8.24,1.17q2.93,1.17,2.93,3.06a2.36,2.36,0,0,1-.23,1.07,4.74,4.74,0,0,1-.51.82,3.23,3.23,0,0,1-.59.59,1.9,1.9,0,0,1-.46.28,6.1,6.1,0,0,0-.89-0.64,10.25,10.25,0,0,0-1.86-.87,19.36,19.36,0,0,0-2.8-.77,19.13,19.13,0,0,0-3.77-.33q-5.92,0-5.92,3.11a2.22,2.22,0,0,0,1.12,2,8.1,8.1,0,0,0,3.21.94l5.2,0.66a13.94,13.94,0,0,1,6.58,2.32,6.07,6.07,0,0,1,2.35,5.18,7.29,7.29,0,0,1-3,6.12q-3,2.3-8.75,2.29a28.1,28.1,0,0,1-5-.41,30.64,30.64,0,0,1-3.9-.94,19.57,19.57,0,0,1-2.68-1,8,8,0,0,1-1.33-.77Z" transform="translate(-49.17 -36.51)"/><path class="cls-2" d="M104.86,131.86H84.24q-8.37,0-12.83-2.79T67,119.94a26.8,26.8,0,0,1,2-9.88,54.14,54.14,0,0,1,5.42-10.2A72.49,72.49,0,0,1,82.2,90.2a70.47,70.47,0,0,1,9.13-8.11,51.9,51.9,0,0,1,9.34-5.58,20.93,20.93,0,0,1,8.59-2.09,15,15,0,0,1,5.64.8q1.88,0.8,2.31,1v44.35h3.54q6.33,0,6.33,5.48a9.75,9.75,0,0,1-.64,3.65,17.57,17.57,0,0,1-1,2.15h-8.27v16.21H104.86V131.86Zm0-11.27V86.65a47.65,47.65,0,0,0-8.16,5.53A70.45,70.45,0,0,0,88.54,100a52.18,52.18,0,0,0-6.28,8.64,16.2,16.2,0,0,0-2.52,7.84q0,2.69,1.93,3.38a15.1,15.1,0,0,0,5,.7h18.15Z" transform="translate(-49.17 -36.51)"/><path class="cls-2" d="M105.29,81.71q0-7.3,7.84-7.3h41.23q6.44,0,6.44,5.8a10.2,10.2,0,0,1-.64,3.7,17.28,17.28,0,0,1-1,2.2H118.29v18.47h34.57q6.33,0,6.34,5.8a11,11,0,0,1-.59,3.7,8.57,8.57,0,0,1-1,2.2h-39.3v20.08h41.88v11.7H105.29V81.71Z" transform="translate(-49.17 -36.51)"/></svg>
					</a>
                    
                    
				
						
			</div>
			<nav class="site-navigation-inner col-sm-9">
				<div class="navbar navbar-default">
				<div class="navbar-header">
				    <!-- .navbar-toggle is used as the toggle for collapsed navbar content -->
				    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
				    	<span class="sr-only">Toggle navigation</span>
				      <span class="icon-bar"></span>
				      <span class="icon-bar"></span>
				      <span class="icon-bar"></span>
				    </button>
				
				    <!-- Your site title as branding in the menu -->
				  </div>

			    <!-- The WordPress Menu goes here -->
	        <?php wp_nav_menu(
                array(
                    'theme_location' => 'primary',
                    'container_class' => 'collapse navbar-collapse navbar-responsive-collapse',
                    'menu_class' => 'nav navbar-nav cl-effect-4',
                    'fallback_cb' => '',
                    'menu_id' => 'Menu 1',
                    'walker' => new wp_bootstrap_navwalker()
                )
            ); ?>
				
				</div><!-- .navbar -->
			</div>
	    </nav><!-- .site-navigation -->
	</div><!-- .container -->
</header><!-- #masthead -->

<div class="main-content ip-content">
	<div class="main-inner-content">
<?php // substitute the class "container-fluid" below if you want a wider content area ?>
	<div id="main-wrapper" class="container-fluid">
			
