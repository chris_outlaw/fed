<?php
/*
Template Name: About Page
*/
get_header(); ?>

<div id="main-image-single" class="row masthead-about">
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<?php echo types_render_field('full-width-image'); ?>

</div>

<div class="row">
	<div class="col-sm-10 col-sm-offset-1">
        <div data-animate-down="ha-header-small" data-animate-up="ha-header-large" class="row skills-roundup ha-waypoint">
        <h2 class="col-sm-12"><?php echo types_render_field('portfolio-page-title'); ?></h2>
        </div>
        <div class="row skills-roundup">
        <?php //the_content(); ?>
        </div>
    </div>
</div>

<div style="background:#f3f3f3;" class="row about-page-info-right">
  
  <div class="col-sm-6 skills-roundup">
    <?php the_content(); ?>
  
  <div class="row">
  <a href="http://www.four-elements-web-design.co.uk/contact/?utm_source=Website&utm_medium=Button&utm_campaign=Start%20your%20project%20-%20Single%20Project%20-<?php echo get_the_title(); ?>&utm_term=Start%20your%20project%20-%20Single%20Project%20-<?php echo get_the_title(); ?>&utm_content=Start%20your%20project%20-%20Single%20Project%20-<?php echo get_the_title(); ?>" class="start-your-project">Start your project</a>
  </div>
  
  </div>
  
  <div class="col-sm-6">
    <?php echo types_render_field('square-feature-image'); ?>
  </div>
  
</div>
</div>

    <?php endwhile; endif; ?>
    
 <div class="my-philosophy container-fluid">
  <div class="row">
    <div class="col-sm-10 col-sm-offset-1">
      <h3>Design Solution</h3>
      <p><?php echo types_render_field('design-brief'); ?></p>
    </div>
  </div>
</div>


<div class="container-fluid">
<div class="row">
  
  <div id="carousel-example-generic" class="carousel slide col-sm-12" data-ride="carousel">
  		<?php
          $tests = get_post_meta($post->ID, 'wpcf-additional-images', false);
		  $count = count($tests);
		  $AI = 1;
		  
		  if($count == 1) {
            foreach ($tests as $test) {?>
            
            <img src="<?php echo $test; ?>" />
            
          <?php $AI++;  } ?> 
		  <?php } else { ?>
          <div class="carousel-inner" role="listbox">
		  <?php foreach ($tests as $test) {?>
            
          <?php if($AI == 1) { ?><div class="item active"><?php } else { ?><div class="item"><?php } ?>
          <img src="<?php echo $test; ?>" />
          
          
          </div>
          <?php } ?> 
          <!-- Controls -->
  	<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  	</a>
  	<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  	</a>
          
          </div>
          <?php } ?>
  </div>
  
  
  </div>
  </div>
  
  <div id="project-outcome" class="row">
  <div class="col-sm-10 col-sm-offset-1">
  	<h3>Project Outcome</h3>
    <p><?php echo types_render_field('project-outcome'); ?></p>
  </div>
  </div>
        
  <div id="portfolio-meta" class="row">
  <div class="col-sm-8 col-sm-offset-2">
  
      <?php if(types_render_field('live-site-url')) { ?>
      <div class="button-group">
      <a href="http://www.four-elements-web-design.co.uk/start-your-project/?utm_source=Website&utm_medium=Button&utm_campaign=Start%20your%20project%20-%20Single%20Project%20-<?php echo get_the_title(); ?>-bottom&utm_term=Start%20your%20project%20-%20Single%20Project%20-<?php echo get_the_title(); ?>-bottom&utm_content=Start%20your%20project%20-%20Single%20Project%20-<?php echo get_the_title(); ?>-bottom" class="start-your-project">Start your project</a>
      <a href="<?php echo types_render_field('live-site-url'); ?>" target="_blank" class="start-your-project">Visit live site</a>
      <?php } else { ?>
      <div class="button-group-one">
      <a href="http://www.four-elements-web-design.co.uk/start-your-projectS/?utm_source=Website&utm_medium=Button&utm_campaign=Start%20your%20project%20-%20Single%20Project%20-<?php echo get_the_title(); ?>-bottom&utm_term=Start%20your%20project%20-%20Single%20Project%20-<?php echo get_the_title(); ?>-bottom&utm_content=Start%20your%20project%20-%20Single%20Project%20-<?php echo get_the_title(); ?>-bottom" class="start-your-project">Start your project</a>
      <?php } ?>
      </div>
  
  </div>
  </div>


<?php
                
        $args = array(
        'post_type' => 'testimonial',
        'posts_per_page' => 1,
		'meta_key' => 'wpcf-portfolio-id',
		'meta_value' => $post->ID,
		'orderby' => 'order',
		'order' => 'ASC'
            );
        
        $loop = new WP_Query( $args );
        
        $count = $loop->post_count;
        
        
        while ( $loop->have_posts() ) : $loop->the_post(); 
		
		if($count >= 1) {
		?>

 <div class="row" id="testimonial">
    	<div class="col-sm-12">
      
      
       <?php the_excerpt(); ?>
       <p class="testimonial-name"><?php the_title(); ?></p>
       
        
        </div>
    </div>

 <?php } else { ?>

 <?php } endwhile; ?>

<?php get_footer();?>
