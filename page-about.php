<?php
/*
Template Name: About Page
*/
get_header(); ?>

<div class="row masthead-about scroll-link">
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<?php //the_post_thumbnail(); ?>
     
     <picture>
<source srcset="<?php echo get_template_directory_uri(); ?>/img/about-bg.jpg" media="(min-width: 1501px)">
<source srcset="<?php echo get_template_directory_uri(); ?>/img/about-bg-large.jpg" media="(min-width: 901px)">
<source srcset="<?php echo get_template_directory_uri(); ?>/img/about-bg-medium.jpg" media="(min-width: 768px)">
<source srcset="<?php echo get_template_directory_uri(); ?>/img/about-bg-standard.jpg" media="(min-width: 601px)">
<source srcset="<?php echo get_template_directory_uri(); ?>/img/about-bg-small.jpg" media="(min-width: 500px)">
<source srcset="<?php echo get_template_directory_uri(); ?>/img/about-bg-extra-small.jpg" media="(min-width: 380px)">
<source srcset="<?php echo get_template_directory_uri(); ?>/img/about-bg-x-x-small.jpg" media="(min-width: 0px)">
<img class="bg" src="<?php echo get_template_directory_uri(); ?>/img/about-bg.jpg"
alt="">
</picture>
     
     <div id="about-intro" class="col-sm-12">   
        <p class="about-intro col-sm-10 col-sm-offset-1">
            I'm <strong>Chris Outlaw</strong> and i enjoy taking <strong>ideas</strong> & making them a <strong>reality</strong></p>
     </div>
     
            <a href="#skills-roundup"><svg id="arrow" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 190 215" style="enable-background:new 0 0 190 215;" xml:space="preserve">
<style type="text/css">
	.st0{fill:none;stroke:#ffffff;stroke-miterlimit:10;}
</style>
<circle class="st0" cx="96" cy="109" r="85"/>
<g>
	<line class="st0" x1="97.5" y1="41" x2="97.5" y2="178"/>
	<line class="st0" x1="97.4" y1="178.2" x2="139" y2="136.6"/>
	<line class="st0" x1="97.1" y1="178.2" x2="55.6" y2="136.6"/>
</g>
</svg>
</a>

</div>

<div data-animate-down="ha-header-small" data-animate-up="ha-header-large" class="row about-page-info-right ha-waypoint">
  
  <div id="skills-roundup" class="col-sm-6 skills-roundup equal">
  
    <h2 class="col-sm-6">A Roundup of <strong>My Skills</strong></h2>
    <p>Four Elements has over 8 years experience with Web Design and Development and a passion for creativity and problem solving.</p>
  
  <div class="row">      
        <ul id="skills-list" class="col-sm-12 unstyled container-fluid">
            <div class="row"><li class="col-sm-12">
            <div class="row"><p class="col-sm-4">HTML5</p> <span class="col-sm-8"></span></div>
            </li></div>
            <div class="row"><li class="col-sm-12"><p class="col-sm-4">CSS3</p> <span class="col-sm-8"></span></li></div>
            <div class="row"><li class="col-sm-12"><p class="col-sm-4">PHP</p> <span class="col-sm-6"></span></li></div>
            <div class="row"><li class="col-sm-12"><p class="col-sm-4">MYSQL</p> <span class="col-sm-5"></span></li></div>
            <div class="row"><li class="col-sm-12"><p class="col-sm-4">Javascript / Jquery</p> <span class="col-sm-6"></span></li></div>
            <div class="row"><li class="col-sm-12"><p class="col-sm-4">Wordpress</p> <span class="col-sm-6"></span></li></div>
            <div class="row"><li class="col-sm-12"><p class="col-sm-4">Creative Suite</p> <span class="col-sm-7"></span></li></div>
        </ul>
  </div>
  
  <div class="row">
  <a href="/work/" class="ghost-button">View Portfolio</a>
  </div>
  
  </div>
  
  <div style="position:relative;" class="col-sm-6 equal">
    <img class="bg bg-absolute" src="<?php echo get_template_directory_uri(); ?>/img/skills-image.jpg" />
  </div>
  
</div>
</div>

    <?php endwhile; endif; ?>
    
 <div class="my-philosophy container-fluid">
  <div class="row">
    <div class="col-sm-10 col-sm-offset-1">
      <h3>My Philosophy</h3>
      <p>Is to help businesses and individuals of all sizes to succeed online, telling your brands story and presenting it simply and concisely to your target audience. I believe in goals, briefs, targets and deadlines and doing everything i can to maximise your brands potential.</p>
      <a href="http://www.four-elements-web-design.co.uk/start-your-project/?utm_source=Four%20Elements%20Site&utm_medium=Button&utm_campaign=About%20Page%20-%20Start%20your%20project&utm_term=About%20Page%20-%20Start%20your%20project&utm_content=About%20Page%20-%20Start%20your%20project" class="start-your-project">Start your project</a>
    </div>
  </div>
</div>

<div class="container-fluid">
<div class="row the-man-behind-the-laptop">
  
  <div style="position:relative;" class="col-sm-6 equal">
    <img class="bg bg-absolute" src="<?php echo get_template_directory_uri(); ?>/img/chris-outlaw-images.jpg" />
  </div>
  
  <div class="col-sm-6 skills-roundup equal">
  
  <div id="the-man" class="row">
    <h2 class="col-sm-12">The Man Behind <strong>The Laptop</strong></h2>
    <p>My name is Chris Outlaw and i love to create beautiful web sites and interfaces that are elegant and simplistic.</p>
    <p>Design and creativity have always been a passion of mine and i truly enjoy helping other to achieve thier goals using these skills. I also am a keen problem solver and enjoy the logical, pragmatic side of Web development building sites, apps and interfaces that are intuitive and engaging.</p>
    <p>I believe these qualities help me to approach projects with both hats on, the creative and the logical which helps to create a balanced end result.</p>
  </div>
        
  <a href="/work/" class="ghost-button">View Portfolio</a>
  
  <div id="social-accounts" class="row-fluid">
    <h2 class="col-sm-12">Social Accounts</h2>
    <ul class="unstyled">
      <li><a href=""><img src="<?php echo get_template_directory_uri(); ?>/img/twitter.jpg" /></a></li>
      <li><a href=""><img src="<?php echo get_template_directory_uri(); ?>/img/facebook.jpg" /></a></li>
      <li><a href=""><img src="<?php echo get_template_directory_uri(); ?>/img/google-plus.jpg" /></a></li>
      <li><a href=""><img src="<?php echo get_template_directory_uri(); ?>/img/linked-in.jpg" /></a></li>
      <li><a href=""><img src="<?php echo get_template_directory_uri(); ?>/img/pinterest.jpg" /></a></li>
    </ul>
  </div>
  
  </div>
  
</div>
</div>


 <div class="row" id="testimonial">
    	<div class="col-sm-12">
        
        <?php
                
        $args = array(
        'post_type' => 'testimonial',
        'posts_per_page' => 1,
		'orderby' => 'rand',
		'order' => 'ASC'
            );
        
        $loop = new WP_Query( $args );
        
        $count = $loop->post_count;
        
        
        while ( $loop->have_posts() ) : $loop->the_post(); ?>
      
      
       <?php the_excerpt(); ?>
       <p class="testimonial-name"><?php the_title(); ?></p>

       
       <?php endwhile; ?>
        
        </div>
    </div>


<?php get_footer();?>
