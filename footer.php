<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package _tk
 */
?>

<footer>

<div class="main-footer">
      <div class="row">
            <div class="col-sm-4 foot-cols">
                  <h5>GET IN TOUCH</h5>
                  <p class="email-info">Email : <a href="mailto:info@four-elements-web-design.co.uk">info@four-elements-web-design.co.uk</a></p>
                  <ul class="social">
                        
                        <li><a class="facebook" href="https://www.facebook.com/FourElementsWebDesign/">facebook</a></li>
                        <li><a class="linked-in" href="uk.linkedin.com/in/fourelementsdesign">linked-in</a></li>
                        <li><a class="pinterest" href="https://uk.pinterest.com/fourelement/">pinterest</a></li>
                        <li class="clear-left"><a class="google-plus" href="https://plus.google.com/u/0/+chrisoutlaw/posts">google plus</a></li>
                        <li><a class="twitter" href="https://twitter.com/FourElementsdes">twitter</a></li>
                        <li><a class="you-tube" href="https://www.youtube.com/channel/UCn1BuNeMnsxbcgdM_FuOwbg">you tube</a></li>
                  </ul>
            </div>
            
            <div class="col-sm-4 foot-cols">
                  <h5>NEWSLETTER</h5>
                  <p class="email-info">
                        Sign up to our the Four Elements
                        newsletter below for occasional
                        ideas, thoughts and goodies.
                  </p>
                  
                  
                  <!-- Begin MailChimp Signup Form -->
<link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
<style type="text/css">
	#mc_embed_signup{clear:left; font:1rem 'open sans',Arial,sans-serif; color:#fff; }
	/* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
	   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
	
	#mc_embed_signup form {
	width:100%;
	float:left;
	margin:0;
	padding:0;	
	}
	
	#mc_embed_signup .mc-field-group label {
	color:#ccc;
	font-family:'open sans';	
	}
	
	#mc_embed_signup .mc-field-group input {
	color:#555;	
	}
	
	@media (max-width: 1699px) {
	
	#mc_embed_signup form { 
	width:90%;
	}
		
	}
	
</style>
<div id="mc_embed_signup">
<form action="//four-elements-web-design.us12.list-manage.com/subscribe/post?u=7a8b8c25126240b20e8492316&amp;id=30e9c119c6" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    <div id="mc_embed_signup_scroll">
	
<div class="indicates-required"><span class="asterisk">*</span> indicates required</div>
<div class="mc-field-group">
	<label for="mce-EMAIL">Email Address  <span class="asterisk">*</span>
</label>
	<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
</div>
<div class="mc-field-group">
	<label for="mce-FNAME">First Name </label>
	<input type="text" value="" name="FNAME" class="" id="mce-FNAME">
</div>
	<div id="mce-responses" class="clear">
		<div class="response" id="mce-error-response" style="display:none"></div>
		<div class="response" id="mce-success-response" style="display:none"></div>
	</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_7a8b8c25126240b20e8492316_30e9c119c6" tabindex="-1" value=""></div>
    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
    </div>
</form>
</div>
<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
<!--End mc_embed_signup-->
                  
                  
            </div>
            
            <div class="col-sm-4 foot-cols">
                  <h5>START YOUR PROJECT</h5>
                  
                  <p class="email-info">
                        Follow the link below to start your
                        project with Four Elements Design.
                  </p>
                  <a href="http://www.four-elements-web-design.co.uk/start-your-project/?utm_source=Website&utm_medium=Button&utm_campaign=Footer%20-%20CTA&utm_term=Footer%20-%20CTA&utm_content=Footer%20-%20CTA" class="start-your-project">Start your project</a>
            </div>
      </div>
      
      <hr>
      
      
      <div class="row">
            <div class="navbar col-sm-6">
            <div class="navbar-inner">
            <ul class="nav col-sm-12">
                  <li><a href="http://www.four-elements-web-design.co.uk">Home</a></li>
                  <li><a href="http://www.four-elements-web-design.co.uk/about/">About</a></li>
                  <li><a href="http://www.four-elements-web-design.co.uk/work/">Work</a></li>
                  <li><a href="http://www.four-elements-web-design.co.uk/services/">Services</a></li>
                  <li><a href="http://www.four-elements-web-design.co.uk/contact/">Contact</a></li>
            </ul>
            </div>
            </div>
           
      </div>
      
      <div class="row-fluid foot-bottom"> 
      <p class="pull-right"><a href="#top">Back to top</a></p>
        <p class="copyright">&copy; <?php bloginfo('name'); ?> 2016 | Email: <a href="mailto:info@four-elements-web-design.co.uk">info@four-elements-web-design.co.uk</a>  | Telephone: <a href="tel:07742753293">07742753293</a></p>
     </div>
      
      </div>
      </div>
      
      </footer>
     

<?php wp_footer(); ?>

      </div><!-- end of ip-container -->
      </div>

<script type="text/javascript">
      
	  jQuery(document).ready(function($) {
            
     	equalheight = function(container){

var currentTallest = 0,
     currentRowStart = 0,
     rowDivs = new Array(),
     $el,
     topPosition = 0;
 $(container).each(function() {

   $el = $(this);
   $($el).height('auto')
   topPostion = $el.position().top;

   if (currentRowStart != topPostion) {
     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
       rowDivs[currentDiv].height(currentTallest);
     }
     rowDivs.length = 0; // empty the array
     currentRowStart = topPostion;
     currentTallest = $el.height();
     rowDivs.push($el);
   } else {
     rowDivs.push($el);
     currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
  }
   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
     rowDivs[currentDiv].height(currentTallest);
   }
 });
}

$(window).load(function() {
  equalheight('.equal');
});

var mz = window.matchMedia("(min-width: 901px)");

if(mz) {

$(window).resize(function(){
  equalheight('.equal');
});

}


var $head = $( '#masthead' );
			$( '.ha-waypoint' ).each( function(i) {
				var $el = $( this ),
					animClassDown = $el.data( 'animateDown' ),
					animClassUp = $el.data( 'animateUp' );

				$el.waypoint( function( direction ) {
					if( direction === 'down' && animClassDown ) {
						$head.attr('class', 'ha-header ' + animClassDown);
					}
					else if( direction === 'up' && animClassUp ){
						$head.attr('class', 'ha-header ' + animClassUp);
					}
				}, { offset: '12%' } );
			} );
			
			

$(function() {
  $('div.scroll-link a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});

	 
      });
     
</script>


  </body>
</html>