<?php
/**
 * Template Name: Page - Project
 * Description: Displays page title and content in Hero section above 3 widgets.
 *
 * @package WordPress
 * @subpackage BootstrapWP
 */
get_header(); ?>

<div class="jumbotron-normal masthead masthead-services">
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    
    <div class="row">
      
      <div id="about-intro" class="col-sm-12">
      <div class="row">
			<h1 class="light">Start <strong>your project</strong></h1>
      </div>
      
      </div>
    </div>
        
</div>


<div id="typeform-form" class="container-fluid">
  <div class="row">
            
            <div class="col-sm-8 col-sm-offset-2">
            <?php the_content(); ?>
            </div>
            
  </div>
</div>


<div id="get-in-touch-mobile" class="container-fluid">
  <div class="row">
    <div class="form-container">
          <div class="row">
            
            <div class="col-sm-7">
            <?php the_content(); ?>
            </div>
            
            <div class="col-sm-5">
              <div class="row"><p class="col-sm-12">Four Elements</p></div>
              <div class="row">
                <div class="col-sm-3"><p>TEL:</p></div>
                <div class="col-sm-9"><p><a href="#">07742753293</a></p></div>
              </div>
              <div class="row">
                <div class="col-sm-3"><p>EMAIL:</p></div>
                <div class="col-sm-9"><p><a class="email" href="mailto:info@four-elements-web-design.co.uk">info@four-elements-web-design.co.uk</a></p></div>
                </div>
                
                <div id="social-accounts-contact" class="row">
                <div class="row"><h3 class="col-sm-12">Connect</h3></div>
                <ul class="row unstyled">
                <li class="col-sm-3"><a href="https://twitter.com/FourElementsdes"><img src="<?php echo get_template_directory_uri(); ?>/img/twitter-social.jpg" alt="Four Elements Twitter" /></a></li>
                <li class="col-sm-3"><a href="https://www.facebook.com/FourElementsWebDesign/"><img src="<?php echo get_template_directory_uri(); ?>/img/facebook-social.jpg" alt="Four Elements Facebook" /></a></li>
                <li class="col-sm-3"><a href="https://plus.google.com/u/0/+chrisoutlaw/posts"><img src="<?php echo get_template_directory_uri(); ?>/img/google-plus-social.jpg" alt="Four Elements Google Plus" /></a></li>
                <li class="col-sm-3"><a href="https://www.youtube.com/channel/UCn1BuNeMnsxbcgdM_FuOwbg"><img src="<?php echo get_template_directory_uri(); ?>/img/you-tube-social.jpg" alt="Four Elements You Tube" /></a></li>
                </ul>
                </div>
                
                
                
            </div>
            
            <?php endwhile; endif; ?>
            
          </div>
    </div>
  </div>
</div>


 <div class="row" id="testimonial">
    	<div class="col-sm-12">
        
        <?php
                
        $args = array(
        'post_type' => 'testimonial',
        'posts_per_page' => 1,
		'orderby' => 'rand',
		'order' => 'ASC'
            );
        
        $loop = new WP_Query( $args );
        
        $count = $loop->post_count;
        
        
        while ( $loop->have_posts() ) : $loop->the_post(); ?>
      
      
       <?php the_excerpt(); ?>
       <p class="testimonial-name"><?php the_title(); ?></p>

       
       <?php endwhile; ?>
        
        </div>
    </div>

<?php get_footer(); ?>